from flask import Flask
import os


app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/human')
def hello_human():
    return 'Hello human!'


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 33507))
    app.run(host='0.0.0.0', port=port)
