import unittest
import Lab1PY


class TestHelloWorld(unittest.TestCase):

    def test_hello(self):
        self.assertEqual(Lab1PY.hello_world(), 'Hello World!')

    def test_human(self):
        self.assertEqual(Lab1PY.hello_human(), 'Hello human!')

if __name__ == '__main__':
    unittest.main()